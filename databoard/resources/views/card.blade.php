<div class="image bg-white rounded-lg shadow-lg overflow-hidden">
    <a href="#">
        <img src="https://cdn.photos.sparkplatform.com/me/20200817153931195510000000-o.jpg">

<div class="px-4 py-4">
        <h2 class=" mb-1 font-bold text-l">South Portland</h2>
        <h6 class="mb-2 text-sm font-medium">635 Commonealth</h6>




        <div class="grid grid-cols-2">
            <ul class="text-xs text-black">
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Status</label><div class="w-12 text-right inline-block">Pending</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Bed</label><div class="w-12 text-right inline-block">3</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Bath</label><div class="w-12 text-right inline-block">1.5</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Density</label><div class="w-12 text-right inline-block">55</div></li>
            </ul>
            <ul class="text-xs text-black">
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Dayn</label><div class="w-12 text-right inline-block">23</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Prtlnd</label><div class="w-12 text-right inline-block">33</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Brnswk</label><div class="w-12 text-right inline-block">8</div></li>
                <li class="leading-5"><label class="text-gray-400 w-12 pr-1 inline-block">Bstn</label><div class="w-12 text-right inline-block">123</div></li>
            </ul>

            </div>
</div>

<div class="text-center rating">
    <span>&#9733;</span>
    <span>&#9733;</span>
    <span>&#9733;</span>
    <span class="bd">&#9733;</span>
    <span>&#9733;</span>
    <span>|</span>
    <span>&#9733;</span>
    <span class="gd">&#9733;</span>
    <span>&#9733;</span>
    <span>&#9733;</span>
    <span>&#9733;</span>
</div>


        <div class="py-6 px-6 text-center tracking-wide grid grid-cols-3 gap-6 _">
            <div class="posts">
                <p class="text-lg">$360K</p>
                <p class="text-gray-400 text-sm">price</p>
            </div>
            <div class="followers">
                <p class="text-lg">$355k</p>
                <p class="text-gray-400 text-sm">estimate</p>
            </div>
            <div class="following">
                <p class="text-lg">1,290</p>
                <p class="text-gray-400 text-sm">ft<sup>2</sup></p>
            </div>
        </div>
    </a>
</div>
