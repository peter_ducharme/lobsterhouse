<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedfinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redfins', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('uuid')->nullable();
            $table->string('ruid')->nullable();
            $table->string('url')->nullable();
            $table->integer('type')->nullable();
            $table->string('name')->nullable();
            $table->string('subName')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();
            $table->boolean('active')->nullable();
            $table->string('img')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('sqrft')->nullable();
            $table->integer('ppsqrft')->nullable();
            $table->integer('beds')->nullable();
            $table->decimal('baths', 2, 1)->nullable();
            $table->integer('year')->nullable();
            $table->integer('dense')->nullable();
            $table->integer('timeOnRedfin')->nullable();
            $table->decimal('predictedValue', 10, 2)->nullable();
            $table->integer('propertyType')->nullable();
            $table->integer('listingType')->nullable();
            $table->integer('boston')->default(0)->nullable();
            $table->integer('portland')->default(0)->nullable();
            $table->integer('brunswick')->default(0)->nullable();
            $table->integer('good')->default(0)->nullable();
            $table->integer('bad')->default(0)->nullable();
            $table
                ->boolean('hide')
                ->default(0)
                ->nullable();
            $table->json('jsn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redfins');
    }
}
