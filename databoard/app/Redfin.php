<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redfin extends Model
{
    protected $fillable = [
        'uuid',
        'ruid',
        'url',
        'type',
        'name',
        'subName',
        'city',
        'zip',
        'active',
        'img',
        'amount',
        'sqrft',
        'ppsqrft',
        'beds',
        'baths',
        'year',
        'dense',
        'timeOnRedfin',
        'predictedValue',
        'propertyType',
        'listingType',
        'boston',
        'portland',
        'brunswick',
        'good',
        'bad',
        'hide',
        'jsn',
    ];
}
