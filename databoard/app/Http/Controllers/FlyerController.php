<?php

namespace App\Http\Controllers;

use App\Flyer;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Mail;
use Google_Service_Gmail;

class FlyerController extends Controller
{
    public function inbox()
    {
        $all = [];

        // BIDGET KING
        //        $html_messages = $this->getMessages('Label_8356157301476299368');
        //        foreach ($html_messages as $html_message) {
        //            $all = array_merge($all, $this->extractHtml($html_message, 'Label_8356157301476299368'));
        //        }

        $html_messages = $this->getMessages('Label_6855886803658889085');
        foreach ($html_messages as $html_message) {
            $all = array_merge($all, $this->extractHtml($html_message, 'Label_6855886803658889085'));
        }

               foreach($all as $each){
                   $uuid = hash("crc32", $each);

                   Flyer::firstOrCreate(
                       ['uuid' => $uuid],
                       ['address' => $each, 'status' => 0]
                   );

               }
        return response()->json(['success' => 'success'], 200);
    }

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Flyer $flyer)
    {
        //
    }

    public function update(Request $request, Flyer $flyer)
    {
        //
    }

    public function destroy(Flyer $flyer)
    {
        //
    }

    private function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Gmail API PHP Quickstart');
        $client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
        $client->setAuthConfig(app_path('../credentials.json'));
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = app_path('../token.json');
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    private function getMessages($folder_id)
    {
        $message_group = [];
        $client = $this->getClient();
        $service = new Google_Service_Gmail($client);
        $user = 'me';

        $optParams = [];
        $optParams['maxResults'] = 5; // Return Only 5 Messages
        $optParams['labelIds'] = $folder_id; // WILL BRIDGET
        $messages = $service->users_messages->listUsersMessages('me', $optParams);
        $lists = $messages->getMessages();

        foreach ($lists as $list) {
            $messageId = $list->getId();
            $optParamsGet = [];
            $optParamsGet['format'] = 'full';
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $parts = $message->getPayload()->getParts();
            foreach ($parts as $part) {
                if ($part->mimeType != 'text/html') {
                    continue;
                }

                $rawData = $part->body->data;
                $sanitizedData = strtr($rawData, '-_', '+/');
                $html = base64_decode($sanitizedData);
                $message_group[] = str_replace("\n\n", "\n", $html);
            }
        }

        return $message_group;
    }

    private function extractHtml($html, $sender)
    {
        $results = [];
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        $doc->loadHTML($html);

        $xpath = new \DOMXpath($doc);
        $articles = $xpath->query('/html/body');

        $inarray = $this->getArray($articles[0]);

        $flat_array = $this->array_flat($inarray);

        $jsttextimg = array_filter(
            $flat_array,
            function ($t) {
                return preg_match('|href|', $t);
                //                return preg_match('|#text|', $t) || preg_match('|\.src|', $t);
            },
            ARRAY_FILTER_USE_KEY
        );



        if ($sender == 'Label_6855886803658889085') {
            foreach ($jsttextimg as $k => $v) {
                list($url, $jnk) = explode('?', $v);
                if (preg_match('/ME/', $v) && !preg_match('/unit/', $url)) {
                $results[] = $url;
                }
            }
        }
        return array_unique($results);
    }

    private function getArray($node)
    {
        $array = false;

        if ($node->hasAttributes()) {
            foreach ($node->attributes as $attr) {
                $array[$attr->nodeName] = $attr->nodeValue;
            }
        }

        if ($node->hasChildNodes()) {
            if ($node->childNodes->length == 1) {
                $array[$node->firstChild->nodeName] = $node->firstChild->nodeValue;
            } else {
                foreach ($node->childNodes as $childNode) {
                    if ($childNode->nodeType != XML_TEXT_NODE) {
                        $valin = $this->getArray($childNode);
                        if ($valin != '') {
                            $array[$childNode->nodeName][] = $valin;
                        }
                    }
                }
            }
        }

        return $array;
    }

    private function array_flat($array, $prefix = '')
    {
        $result = [];

        foreach ($array as $key => $value) {
            $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

            if (is_array($value)) {
                $result = array_merge($result, $this->array_flat($value, $new_key));
            } else {
                $result[$new_key] = $value;
            }
        }

        return $result;
    }
}
